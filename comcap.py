#! /usr/bin/env python
"""
This is a simple mechanize script that logs into Comcast, retrieves the usage
information, and prints it to stdout.

Requires robobrowser.

Copyright (C) 2014 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import sys
from getpass import getpass
from netrc import netrc

from robobrowser.browser import RoboBrowser

LOGIN = 'https://login.comcast.net/login?' +\
        'continue=%2fMyServices%2fInternet%2f&s=ccentral-cima&r=comcast.net'
SERVICES = 'https://customer.comcast.com/MyServices/Internet'
PRELOADER = 'https://customer.comcast.com/Secure/Preloader.aspx'
AJAX = 'https://customer.comcast.com/MyServices/Internet?ajax=1'


def debug(t, v, tb):
    import pdb
    import traceback
    traceback.print_exception(t, v, tb)
    print
    pdb.pm()

sys.excepthook = debug


def getBrowser():
    br = RoboBrowser(
        history=True,
        user_agent='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_1) '
                   'AppleWebKit/534.48.3 (KHTML, like Gecko) '
                   'Version/5.1 Safari/534.48.3'
    )
    return br


def login(br):
    try:
        n = netrc()
        username, account, password = n.authenticators('login.comcast.net')
        if None in (username, password):
            raise
    except:
        username = raw_input('Username: ')
        password = getpass()
    br.open(LOGIN)
    form = br.get_form(attrs={'name': 'signin'})
    form['user'].value = username
    form['passwd'].value = password
    br.submit_form(form)
    try:
        form = br.get_form(attrs={'name': 'signin'})
        form['passwd'].value = password
        br.submit_form(form)
    except:
        pass
    try:
        form = br.get_form(attrs={'name': 'redir'})
    except:
        raise Exception('Failed to login')
    br.submit_form(form)


def printBandwidthUsage(br):
    br.open(PRELOADER)
    br.open(SERVICES)
    data = {
        'ajax': 1,
        'control': ['LoadUsageMeter']
    }
    response = br.session.post(AJAX, data=data)
    br._update_state(response)
    try:
        usagetext = br.select('.cui-meter-details-info')[0].text
        print usagetext
    except:
        try:
            print br.select('.cui-message-content')[0].text
        except:
            raise Exception('Failed to find bandwidth usage.')


def main():
    br = getBrowser()
    login(br)
    printBandwidthUsage(br)


if __name__ == '__main__':
    main()

