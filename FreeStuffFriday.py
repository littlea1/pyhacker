# coding=utf-8
"""
This script enters the Free Stuff Friday contest that Sega does on Fridays.
Requires that the user already follows @Sega on twitter.

Requires oauthtwitter

Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import re
import os
import sys
import time
import cPickle as pickle

import oauthtwitter as twitter

consumer_key = '' # Paste consumer key here
consumer_secret = '' # Paste consumer secret here

fract = 10.0 # We'll wait a number of seconds divided by this number
numString = re.compile(r'([0-9\s][0-9][nrst][dht])|(\#[0-9]+)|(DM\ [0-9]+)')
quoteString = re.compile(r'["/*]([^"/*]+)["/*]')
dmString = re.compile(r'^(.*)DM(.*)["/*](.+)["/*](.*)$')

def login():
    try:
        with open('.freestufffriday', 'rb') as f:
            access_token = pickle.load(f)
    except:
        api = twitter.OAuthApi(consumer_key, consumer_secret)
        request_token = api.getRequestToken()
        authorization_url = api.getAuthorizationURL(request_token)
        print "Go here to authorize: %s" % authorization_url
        pin = raw_input('Enter pin: ')
        api = twitter.OAuthApi(consumer_key, consumer_secret, request_token)
        access_token = api.getAccessToken(pin=pin)
        with open('.freestufffriday', 'wb') as f:
            pickle.dump(access_token, f)
    api = twitter.OAuthApi(consumer_key, consumer_secret, access_token)
    api.SetCache(None)
    return api

def main(prize, iwant):
    api = login()
    nexttweet = False
    giveaway = False
    sleep = 60
    wait = 0.0
    dmsSent = set()
    while True:
        try:
            sega = api.GetUser('sega')
            ratelimit = api.RateLimitStatus()
        except Exception as e:
            print e
            print "Remaininig hits: %d/%d, will reset in %.2f seconds" % \
                  (ratelimit['remaining_hits'],ratelimit['hourly_limit'],(ratelimit['reset_time_in_seconds']-time.time()))
            if ratelimit['remaining_hits'] == 0:
                waittime = ((ratelimit['reset_time_in_seconds']-time.time())+2)
                print "Waiting %d seconds and then trying again." % waittime
                time.sleep(waittime)
                time.sleep(60)
                sega = api.GetUser('sega')
                ratelimit = api.RateLimitStatus()
        status = sega.GetStatus()
        text = status.GetText()
        text = text.encode("utf-8")
        createdat = status.GetCreatedAtInSeconds()
        if nexttweet or giveaway:
            fquotes = quoteString.search(text)
	    if createdat > oldcreatedat or fquotes:
                if prize:
                    iwant = False
                    for p in prize:
                        if text.lower().find(p) != -1:
                            iwant = True
                            break
                if not iwant:
                    print "I don't want this prize. Waiting for next one."
                    nexttweet = False
                    sleep = 60
                    time.sleep(sleep)
                else:
                    if not fquotes or 'http://%s' % fquotes.group()[1:-1] in text:
                        nexttweet = False
                        sleep = 60
                        print "Sleeping for %d seconds" % sleep
                        time.sleep(sleep)
                    else:
                        text = fquotes.group()[1:-1]
                        print 'Sleeping for %f seconds' % wait
                        time.sleep(wait)
                        if text not in dmsSent:
                            api.PostDirectMessage('sega',text) # Send direct message
	                    nexttweet = False
	                    sleep = 300
	                    print 'Sent Direct Message to Sega: %s' % text
                            wait = 0.0
                        dmsSent.add(text)
        # Sega Europe uses the next tweet method
        if text.lower().find('next tweet') != -1 or \
                text.lower().find('responder') != -1 or \
                text.lower().find('phrase next') != -1:
	    nexttweet = True
	    sleep = 5
        # Sega USA uses the giveaway method
        if text.upper().find('GIVEAWAY') != -1 or dmString.search(text.upper()):
	    giveaway = True
	    sleep = 0
        a = numString.search(text)
        if a:
            print 'Need to be the %s person to win' % a.group()
            if '#' in a.group():
                wait = float(a.group().replace('#', '')) / fract
            elif 'DM' in a.group():
                wait = float(a.group().replace('DM', '')) / fract
            else:
                wait = float(a.group()[:-2]) / fract
        else:
            giveaway = False
            nexttweet = False
            sleep = 60
        oldcreatedat = createdat
        print "Remaininig hits: %d/%d, will reset in %.2f seconds" % \
              (ratelimit['remaining_hits'],ratelimit['hourly_limit'],(ratelimit['reset_time_in_seconds']-time.time()))
        try:
            print "Current tweet: %s" % text
        except:
            pass
        print "Direct message next tweet? %s" % (nexttweet or giveaway)
        print "Sleeping for %d seconds" % sleep
        time.sleep(sleep)
        sleep = 60

if __name__ == '__main__':
    if len(sys.argv) == 1:
        prize = False
        iwant = True
    else:
        prize = sys.argv[1:]
        iwant = False
    main(prize, iwant)
    
