#! /usr/bin/env python
"""
Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import smtplib
import optparse
from netrc import netrc
from getpass import getpass
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import Encoders

def main(options, args):
    message = options.message
    subject = options.subject
    attachments = options.attach
    if '@' not in args[0]: # Read addresses from file
        sendto = []
        with open(args[0], 'r') as f:
            for address in f:
                sendto.append(address.strip())
    else:
        sendto = args[0].split(',')
    try:
        n = netrc()
        username, account, password = n.authenticators('smtp.gmail.com')
        if None in (username, password):
            raise
    except:
        username = raw_input('Gmail address: ')
        password = getpass('Gmail password: ')
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = username
    msg['To'] = ','.join(sendto)
    if options.cc:
        msg['CC'] = ','.join(options.cc)
        sendto += options.cc
    if options.bcc:
        msg['BCC'] = ','.join(options.bcc)
        sendto += options.bcc
    msg.attach(MIMEText(message.replace('\\n', '\n')))
    for a in attachments:
        part = MIMEBase('application', 'octet-stream')
        with open(a, 'rb') as f:
            part.set_payload(f.read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(a))
        msg.attach(part)
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(username, password)
    s.sendmail(username, sendto, msg.as_string())
    s.quit()

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.set_usage("""%prog <toaddress> [OPTIONS]
  Send an email from your Gmail account to <toaddress>.
  <toaddress> can also be a comma-separated list of addresses.
  Use -m for message and -s for subject.
  See --help for more details.""")
    parser.add_option('--message', '-m', action='store', default='',
                      help="""The email message body.""")
    parser.add_option('--subject', '-s', action='store', default='',
                      help="""The email subject.""")
    parser.add_option('--attach', '-a', action='append', default=[],
                      help="""Attach a file.""")
    parser.add_option('--cc', '-c', action='append', default=[],
                      help="""Add an address to the CC field.""")
    parser.add_option('--bcc', '-b', action='append', default=[],
                      help="""Add an address to the BCC field.""")
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.print_usage()
        sys.exit(1)
    main(options, args)
