#!/usr/bin/env python
"""
This script converts a Disqus xml export to generic WXR format. Useful if you
want to migrate your comments from Disqus to Wordpress or to another Disqus
site.

Requires unidecode.

Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import re
import sys
from collections import defaultdict
from xml.dom.minidom import parseString
from xml.etree.ElementTree import iterparse

from unidecode import unidecode

def debug(t, v, tb):
    import traceback, pdb
    traceback.print_exception(t, v, tb)
    print
    pdb.pm()

sys.excepthook = debug

class Comment(object):
    def __init__(self, thread, threadId, id, postId, message, parent, parentId,
                 createdAt, ipAddress, authorName, authorEmail):
        self.thread = thread
        self.threadId = threadId
        self.id = id
        self.postId = postId
        self.message = message
        self.parent = parent
        self.parentId = parentId
        self.createdAt = createdAt
        self.ipAddress = ipAddress
        self.authorName = authorName
        self.authorEmail = authorEmail

def mapc(*l, **kw):
    f = l[0]
    for i in zip(*l[1:]):
        f(*i, **kw)

def flatten(l):
    r = []
    mapc(lambda i: r.extend(i) if isinstance(i, list) else r.append(i), l)
    return r

def getComments(threadId, posts):
    c = []
    for post in posts.pop(threadId, []):
        postId = post.attrib.values()[0]
        parent = None
        parentId = None
        for el in post.getchildren():
            if el.tag.endswith('id'):
                id = el.text
            elif el.tag.endswith('message'):
                message = el.text
            elif el.tag.endswith('thread'):
                thread = el.text
            elif el.tag.endswith('ipAddress'):
                ipAddress = el.text
            elif el.tag.endswith('createdAt'):
                createdAt = el.text
            elif el.tag.endswith('author'):
                for el2 in el.getchildren():
                    if el2.tag.endswith('username'):
                        continue
                    elif el2.tag.endswith('name'):
                        authorName = el2.text
                    elif el2.tag.endswith('email'):
                        authorEmail = el2.text
            elif el.tag.endswith('parent'):
                parent = el.text
                parentId = el.attrib.values()[0]
        c.append(Comment(thread, threadId, id, postId, message, parent, parentId,
                         createdAt, ipAddress, authorName, authorEmail))
    return c

def addElement(node, el, name):
    n = ['<%s>' % name]
    if el is None:
        v = ''
    else: 
        v = unidecode(el.replace('&', '&amp;')).replace('<', '&lt;').replace('>', '&gt;')
    n.append(v)
    n.append('</%s>' % name)
    node.append(n)

def makeText(node):
    t = ["""<rss version="2.0"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dsq="http://www.disqus.com/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:wp="http://wordpress.org/export/1.0/">"""]
    t.extend(flatten(node))
    t.append('</rss>')
    t = flatten(t)
    final = []
    for y in parseString('\n'.join(t)).firstChild.childNodes:
        y = y.toxml()
        if y == '\n':
            continue
        tmp = []
        space = 6
        for x in y.split('\n'):
            if x.startswith(('<item>', '</item>')):
                space = 4
            tmp.append((' ' * (space + (0 if x.startswith('<') else 2))) + x)
            if x.startswith(('<item>', '</item>')):
                space = 6
            elif x.startswith('<wp:comment>'):
                space = 8
            elif x.startswith('</wp:comment>'):
                space = 6
        final.append('\n'.join(tmp))
    return ''.join(final)

def addThread(thread, posts):
    """
    Add a thread and any comments to the output DOM.
    """
    threadId = thread.attrib.values()[0]
    for el in thread.getchildren():
        if el.tag.endswith('id'):
            id = el.text
        elif el.tag.endswith('forum'):
            forum = el.text
        elif el.tag.endswith('category'):
            category = el.attrib.values()[0]
        elif el.tag.endswith('link'):
            link = el.text
        elif el.tag.endswith('title'):
            title = el.text
        elif el.tag.endswith('message'):
            message = el.text
        elif el.tag.endswith('createdAt'):
            createdAt = el.text
        elif el.tag.endswith('author'):
            for el2 in el.getchildren():
                if el2.tag.endswith('username'):
                    continue
                elif el2.tag.endswith('name'):
                    authorName = el2.text
                elif el2.tag.endswith('email'):
                    authorEmail = el2.text
        elif el.tag.endswith('isClosed'):
            isClosed = el.text
            if isClosed == 'false':
                isClosed = 'open'
            else:
                isClosed = 'closed'
    comments = getComments(threadId, posts)
    item = ['<item>']
    addElement(item, title, 'title')
    addElement(item, link, 'link')
    addElement(item, threadId, 'dsq:thread_identifier')
    addElement(item, ' '.join(createdAt.split('T')).replace('Z', ''), 'wp:post_date_gmt')
    addElement(item, isClosed, 'wp:comment_status')
    for comment in comments:
        c = ['<wp:comment>']
        addElement(c, comment.postId, 'wp:comment_id')
        addElement(c, comment.authorName, 'wp:comment_author')
        addElement(c, comment.authorEmail, 'wp:comment_author_email')
        addElement(c, comment.ipAddress, 'wp:comment_author_IP')
        addElement(c, ' '.join(comment.createdAt.split('T')).replace('Z', ''), 'wp:comment_date_gmt')
        addElement(c, comment.message, 'wp:comment_content')
        addElement(c, '1', 'wp:comment_approved')
        if comment.parentId:
            addElement(c, comment.parentId, 'wp:comment_parent')
        c.append('</wp:comment>')
        item.append(c)
    item.append('</item>')
    return makeText(item)

def main(args):
    if len(args) == 1:
        output = os.path.splitext(args[0])[0] + '_wxr.xml'
    else:
        output = args[1]
    input = args[0]
    posts = defaultdict(list)
    for el in iterparse(input):
        if el[1].tag.endswith('post') and el[1].getchildren():
            post = el[1]
            tid = [x.attrib.values()[0] for x in post.getchildren() if x.tag.endswith('thread')]
            if not tid:
                continue
            posts[tid[0]].append(el[1])
    out = """<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dsq="http://www.disqus.com/"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:wp="http://wordpress.org/export/1.0/">
  <channel>\n"""
    with open(output, 'w') as f:
        f.write(out)
        for thread in iterparse(input):
            if thread[1].tag.endswith('thread') and thread[1].getchildren():
                f.write('%s\n' % addThread(thread[1], posts))
        f.write('  </channel>\n</rss>\n')

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: %s <disqus xml export> [wxr output file]" % sys.argv[0]
        sys.exit(1)
    main(sys.argv[1:])

